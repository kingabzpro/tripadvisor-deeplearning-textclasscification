![Picture title](Image/image-20210418-182652.png)

Image Credit: [wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Tripadvisor_Logo_circle-green_vertical-lockup_registered_RGB.svg/1200px-Tripadvisor_Logo_circle-green_vertical-lockup_registered_RGB.svg.png)

# Introduction
In this project, we will be exploring the hotel reviews and the rating base on customer hotel experience. We will be also looking at feature engineering and designing a deep learning model to predict ratings based on reviews. We will be using NLP tools for feature extractions and preparing the data for deep learning models.
## About Tripadvisor
**Tripadvisor, Inc.** is an American online travel company that operates a website and mobile app with user-generated content and a comparison shopping website. It also offers online hotel reservations and bookings for transportation, lodging, travel experiences, and restaurants. Its headquarters are in Needham, Massachusetts. [Wikipedia](https://en.wikipedia.org/wiki/Tripadvisor)
## About this dataset
Hotels play a crucial role in traveling and with the increased access to information new pathways of selecting the best ones emerged.
With this dataset, consisting of 20k reviews crawled from Tripadvisor, you can explore what makes a great hotel and maybe even use this model in your travels!

**The Tripadvisor Dataset is available publicly available on [Zenodo](https://zenodo.org/record/1219899#.YHwt1J_ivIU)**

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1219899.svg)](https://doi.org/10.5281/zenodo.1219899) 

AttributionNon-Commerical

Attribution-NonCommercial 4.0 International
## Todolist
- [x] Preprocessing (Vader)
- [x] Data Exploration
- [x] Data Visualization
- [x] WordClouds
- [x] Adding Keywords (Gensim)
- [x] Text Processing (NLTK)
- [x] Creating BiLSTM Model
- [X] Hyperparameters Tuning
- [x] Train and validation
- [x] Metrics / Model Performance
- [X] Prediction
- [x] Saving Model
- [x] Final thoughts
